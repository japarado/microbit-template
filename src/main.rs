#![no_main]
#![no_std]

extern crate panic_rtt_target;

use core::fmt::Write;
use cortex_m_rt::entry;
use microbit::hal::uarte::{Baudrate, Parity};
use microbit::hal::Uarte;
use microbit::Board;
use rtt_target::rtt_init_print;

use crate::serial::UartePort;

mod serial;

#[entry]
fn main() -> !
{
    rtt_init_print!();

    let mut board = Board::take().unwrap();

    let mut serial = {
        let serial = Uarte::new(
            board.UARTE0,
            board.uart.into(),
            Parity::EXCLUDED,
            Baudrate::BAUD115200,
        );
        UartePort::new(serial)
    };

    loop
    {}
}
